# sounds

meta-repository to git clone all my audio, sounds and samples repositories

the samples can be used on live coding sessions with tidal cycles, dublang or
any other tool

## repositories

- https://gitlab.com/joenio/sounds-atari2600
- https://gitlab.com/joenio/sounds-berimbau
- https://gitlab.com/joenio/sounds-furby
- https://gitlab.com/joenio/sounds-mocreo
- https://gitlab.com/joenio/sounds-pandeiro
- https://gitlab.com/joenio/synths-puredata
- https://gitlab.com/joenio/sounds-reggae

## dub synths, sounds, notes and references

### tape echo effect

- supercollider effects?
- tidalcycles effects?
- others free software tools?

tape echo demo:
- https://www.youtube.com/watch?v=Svm43r8RZFI

### spring reverb effect

- supercollider, tidal, others?

spring reverb demo:
- https://www.youtube.com/watch?v=tU7U-U-n4EQ
- https://www.youtube.com/watch?v=vIYR0MLqqGs

### melodica instrument

- supercollider, tidal, others?

melodica demo:
- https://www.youtube.com/watch?v=mtLgzmxXVA8

#### Snare drum reggae, dub, instrument

TODO

### wah, okverdrive, phaser pedals

- guitarrix?

## references

- [The beginner's guide to: dub](https://www.musicradar.com/news/the-beginners-guide-to-dub)
- [New documentary celebrates life and legacy of dub pioneer Lee “Scratch” Perry: Watch](https://djmag.com/news/new-documentary-celebrates-life-and-legacy-dub-pioneer-lee-scratch-perry-watch)
- [Reggae documentaries: 10 best documentaries about reggae](https://enkismusicrecords.com/best-reggae-documentaries)

## movies and docs

- [Studio One Story (Documentary)](https://www.youtube.com/watch?v=QggBzx9smC4)
- [Deep Into Dub - Lost in Music - 1997 Full Documentary](https://m.facebook.com/rootsshed/videos/deep-into-dub-lost-in-music-1997-full-documentary-english-language/310633333295362/)

## see also

- [Where to find samples?](https://tidalcycles.org/docs/configuration/AudioSamples/find_samples)

## author

Joenio Marques da Costa <<joenio@joenio.me>>

## license

This project is licensed under the GNU General Public License v3.0 or later -
see the [COPYING](/COPYING) file for details
